Source: frogdata
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Ko van der Sloot <ko.vandersloot@uvt.nl>,
           Maarten van Gompel <proycon@anaproy.nl>
Section: science
Priority: optional
Build-Depends: cdbs, debhelper (>= 10)
Standards-Version: 4.5.0
Vcs-Git: https://salsa.debian.org/science-team/frogdata.git
Vcs-Browser: https://salsa.debian.org/science-team/frogdata
Homepage: http://languagemachines.github.io/frog/

Package: frogdata
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends}
Replaces: frog (<< 0.12.15)
Multi-Arch: foreign
Description: Data files for Frog
 Frog is a modular system integrating a morphosyntactic tagger, lemmatizer,
 morphological analyzer, and dependency parser for the Dutch language.
 .
 This package provided necessary datafiles for running Frog.
 .
 Frog is a product of the Centre for Language and Speech Technology 
 (Radboud University, Nijmegen) and prior to that of ILK Research Group
 (Tilburg University, The Netherlands) and the CLiPS Research Centre 
 (University of Antwerp, Belgium). It is currently maintained at the
 KNAW Humanities Cluster.
